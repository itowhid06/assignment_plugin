<?php
/*
  Plugin Name: Assignment by Towhid
  Plugin URI:
  Description: Solution to the assignment work given by Chhuti via email.
  Version: 0.1
  Author: Towhidul Islam
  Author URI: https://github.com/itowhid06
  License: GPLv2
 */

// Add a taxonomy "review" which is associated with our custom post type "diary"

function chhuti_assignment_taxonomy() {

    $labels = array(
        'name' => __('Review', 'my_custom_plugin'),
        'label' => __('Review', 'my_custom_plugin'),
        'singular_name' => __('Review', 'my_custom_plugin'),
        'search_items' => __('Search Reviews', 'my_custom_plugin'),
        'all_items' => __('All Reviews', 'my_custom_plugin'),
        'edit_item' => __('Edit Reviews', 'my_custom_plugin'),
        'update_item' => __('Update Reviews', 'my_custom_plugin'),
        'add_new_item' => __('Add New Review', 'my_custom_plugin'),
        'new_item_name' => __('New Review Name', 'my_custom_plugin'),
        'menu_name' => __('Reviews', 'my_custom_plugin'),
    );

    $args = array(
        'labels' => $labels,
        'label' => __('Location Review', 'my_custom_plugin'),
        'hierarchical'      =>  true,
        'show_ui' => true,
        'show_admin_column' => true
    );
    register_taxonomy('review', array('diary'), $args);
}

add_action('init', 'chhuti_assignment_taxonomy');

// Add a custom post type "diary" which is associated with our custom taxonomy "review"

function diary_setup() {
    
    $labels = array(
        'name' => __('Diary', 'my_custom_plugin'),
        'singular_name' => __('Diary', 'my_custom_plugin'),
        'add_new_item' => __('Add New Diary', 'my_custom_plugin'),
        'edit_item' => __('Edit Diary', 'my_custom_plugin'),
        'new_item' => __('New Diary', 'my_custom_plugin'),
        'not_found' => __('No Diary found', 'my_custom_plugin'),
        'all_items' => __('All Diary', 'my_custom_plugin')
    );
    $args = array(
        'labels' => $labels,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'has_archive' => true,
        'map_meta_cap' => true,
        'menu_icon' => 'dashicons-format-aside',
        'supports' => array('title', 'editor', 'thumbnail', 'author'),
        'taxonomies' => array('review')
    );
    register_post_type('diary', $args);
}

add_action('init', 'diary_setup');

// Add a post meta where author can select a city from drop down

function diary_add_meta_boxes($post) {
    add_meta_box('diary_meta_box', __('Select City', 'my_custom_plugin'), 'diary_build_meta_box', 'diary', 'normal', 'high');
}

add_action('add_meta_boxes_diary', 'diary_add_meta_boxes');

function diary_build_meta_box($post) {

    wp_nonce_field(basename(__FILE__), 'diary_meta_box_nonce');
    ?>
    <div class='inside'>

        <select name="meta-box-dropdown">
            <?php
            $option_values = array('Dhaka', 'London', 'Paris', 'Texas');

            foreach ($option_values as $key => $value) {
                    ?>
                    <option><?php echo $value ; ?></option>
                    <?php
            }
            ?>
        </select>
    </div>
    <?php
}

function diary_save_meta_box_data($post_id) {

    if (!isset($_POST['diary_meta_box_nonce']) || !wp_verify_nonce($_POST['diary_meta_box_nonce'], basename(__FILE__))) {
        return;
    }

    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return;
    }

    if (!current_user_can('edit_post', $post_id)) {
        return;
    }

    $meta_box_dropdown_value = "";

    if (isset($_POST["meta-box-dropdown"])) {
        $meta_box_dropdown_value = $_POST["meta-box-dropdown"];
        update_post_meta($post_id, "meta-box-dropdown", $meta_box_dropdown_value);

        // Some more post meta along with the weather information of selected city, collected from any "apixu" weather API. The weather info filed is not visible to the author.

        $key = "e2fe7b82d42e418eab940036172901";
        $city = "$meta_box_dropdown_value";
        $url = "http://api.apixu.com/v1/current.json?key=$key&q=$city&=";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $json_output = curl_exec($ch);
        $weather = json_decode($json_output);

        update_post_meta($post_id, "author-country-meta", $weather->location->country);

        update_post_meta($post_id, "author-city-temp", $weather->current->temp_c);
    }
}

add_action('save_post_diary', 'diary_save_meta_box_data');

function meta_limit_increase($limit) {
    return 50;
}

add_filter('postmeta_form_limit', 'meta_limit_increase');

//Few taxonomy meta generated for our custom taxonomy "review"

function add_some_term_metas() {

    $taxonomy_name = 'review';
    $term_args = array(
        'orderby' => 'name',
        'hide_empty' => false,
        'fields' => 'ids'
    );
    $terms = get_terms($taxonomy_name, $term_args);

    if ($terms) {
        foreach ($terms as $term_id) {
            update_term_meta($term_id, 'non_unique_key1', 'test_term_meta_1');
            update_term_meta($term_id, 'non_unique_key2', 'test_term_meta_2');
            update_term_meta($term_id, 'non_unique_key3', 'test_term_meta_3');
        }
    }
}

add_action('init', 'add_some_term_metas');
?>